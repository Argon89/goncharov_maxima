﻿namespace web_api; //Middlewares

using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

public class ExceptionCustomMiddlewares //: IMiddleware
{
    private RequestDelegate _next;
    private readonly ILogger<ExceptionCustomMiddlewares> _logger;

    public ExceptionCustomMiddlewares(RequestDelegate next, ILogger<ExceptionCustomMiddlewares> logger) //, Logger<ExceptionCustomMiddlewares> logger
    {
        _next = next;
        _logger = logger;
    }

    


    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await _next.Invoke(context);
        }
        catch (Exception e)
        {
            _logger.LogError("Exception in the ExceptionCustomMiddlewares !!! {0}", context.Request.Path);
            Console.WriteLine(e);
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            await context.Response.WriteAsync(e.Message);
            await context.Response.WriteAsync("// Error 500. An exception occurred for a task in HW=28!");
        }
    }
}

/*
 
 private readonly RequestDelegate _next;
    // private readonly ILogger _logger;
    
    public ExceptionCustomMiddlewares (RequestDelegate next)
    {
        _next = next;
        
    }
 
 
 * public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await _next.Invoke(context);
        }
        catch (Exception e)
        {
            
            // _logger.LogError(e.ToString());
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            await context.Response.WriteAsync(e.Message);
        }
    }
 */