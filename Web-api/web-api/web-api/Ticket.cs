﻿namespace web_api;

public class Ticket
{
    public string Movie { get; set; }
    public string Genre { get; set; }
    public string AgeRating { get; set; }
    public int OrderNumber { get; set; }
    public DateTime Time { get; set; }
    public int TicketPrice { get; set; }
    public int Hall { get; set; }
    public int Row { get; set; }
    public int SeatNumber { get; set; }
}

/*
 Класс Ticket должен содержать
○ Наименование фильма
○ Жанр и возрастной рейтинг
○ Номер заказа
○ Время сеанса
○ Стоимость билета
○ Номер зала
○ Место (ряд, место)
● Класс TicketController должен содержать методы
○ GET ([HttpGet]) метод, позволяющий получать все билеты (данные можно заполнять рандомно)

*/