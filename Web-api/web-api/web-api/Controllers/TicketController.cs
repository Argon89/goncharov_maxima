﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
namespace web_api.Controllers;

[ApiController]
[Route("[controller]")] 
public class TicketController: ControllerBase
{
    private static readonly string [] AgeRatings  = new[]  { "0+", "6+", "12+", "16+", "18+" };
    private static readonly string [] Genres = new[] { "Боевик", "Мультфильм", "Детектив", "Фантастика", "Триллер", "Фильм ужасов", "Комедия" };
    private static readonly string [] Movies = new[] { "Крепкий орешек", "Алёша Попович и Тугарин змей", "Шерлок Холмс и доктор Ватсон", "Матрица", "Кошмар на улице вязов", "Гарри Поттер" };
    
    private readonly ILogger<TicketController> _logger;
    public TicketController(ILogger<TicketController> logger)
    {
        _logger = logger; //   CreateLogger("MyCategory");
    }

    [HttpGet("GetTicket")]
    public IEnumerable<Ticket> GetTicket()
    {
        _logger.LogInformation("A request was received at the URL:{0}", Request.Path);
        int rndExcept = Random.Shared.Next(0, 100000) % 2;

        if (rndExcept != 0)
        {
            _logger.LogError("Random exception in the GetTicket() method!!!{0}", Request.Path);
            throw new Exception("Something went wrong!!!");
        }
        
        
        return Enumerable.Range(1, 7).Select(x => new Ticket
            {
                Movie = Movies[Random.Shared.Next(Movies.Length)],
                Genre = Genres[Random.Shared.Next(Genres.Length)],
                AgeRating = AgeRatings[Random.Shared.Next(AgeRatings.Length)],
                Time = DateTime.Now.AddDays(Random.Shared.Next(-360,360)),
                OrderNumber = Random.Shared.Next(1, 1500),
                TicketPrice = Random.Shared.Next(100, 1500),
                Hall = Random.Shared.Next(1, 3),
                Row = Random.Shared.Next(1, 20),
                SeatNumber = Random.Shared.Next(1, 25),
                
            })
            .ToList();
        
    }
    
    [HttpGet("Exception")]
    public void GetExcept()
    {
        _logger.LogInformation("A request was received at the URL:{0}", Request.Path);
        _logger.LogError("Exception in the GetExcept() method!!!");
        throw new Exception();
    }
}
/*
public AboutModel(ILogger<AboutModel> logger)
    {
        _logger = logger;
    }

*/
























/*
 
Азиатский экстрим
Аналоговый хоррор
Анимационно-игровой фильм
Б
Бадди-муви
Фильм-балет
Биографический фильм
Боевик (киножанр)
Фильм о боевых искусствах
Бромантическая комедия
В
Вестерн
Военный фильм
Г
Гангстерский фильм
Д
Детективный фильм
Детское кино
Дзидайгэки
Дневниковое кино
Документальная анимация
Драматический фильм
З
Закадровая съёмка
Зомби-комедия
И
Истерн
Историческая драма
Исторический фильм
История взросления
Итальянская эротическая комедия
К
Фильм-катастрофа
Кино белых телефонов
Киноальманах
Кинокомедия
Киноповесть
Кинофантастика
Комедийная драма
Комедийный триллер
Комедия о возобновлении брака
Комедия по-итальянски
Комедия положений
Комедия ужасов
Концертный фильм
Криминальная драма
Криминальный фильм
М
Мамблкор
Масала (жанр)
Мелодрама
Метакино
Музыкальный фильм
Н
Найденная плёнка
Научно-фантастический фильм
Независимый кинематограф
Нуар
П
Пеплум (жанр)
Подростковый фильм
Политический триллер
Порнофильм
Постапокалиптика
Приключенческий фильм
Производственный фильм
Пропагандистский фильм
Псевдодокументальный фильм
Психологическая драма
Психологический триллер
Р
Романтическая комедия
Романтический фильм
Романтическое фэнтези
С
Семейный фильм
Фильм-сказка
Скринлайф
Т
Фильм-тайна
Танцевальный фильм
Триллер
Тямбара
У
Украинское поэтическое кино
Ф
Фарс
Фильм категории B
Фильм категории C
Фильм категории Z
Фильм о друзьях-полицейских
Фильм о зомби
Фильм ужасов
Фильм-выживание
Фильм-спектакль
Фолк-хоррор
Фэнтезийная комедия
Ч
Чёрный юмор
Чикфлик
Чистое кино
Ш
Шпионский фильм
Э
Эксплуатационное кино
Эксцентрическая комедия
Эпический фильм
Ю
Юридическая драма
Юридический триллер
Я
Якудза эйга
 
 
 
 Класс Ticket должен содержать
○ Наименование фильма
○ Жанр и возрастной рейтинг
○ Номер заказа
○ Время сеанса
○ Стоимость билета
○ Номер зала
○ Место (ряд, место)
● Класс TicketController должен содержать методы
○ GET ([HttpGet]) метод, позволяющий получать все билеты (данные можно заполнять рандомно)

public class TicketController : Controller
{
    // GET
    public IActionResult Index()
    {
        return View();
    }
}*/