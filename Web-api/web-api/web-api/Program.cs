using web_api;

var builder = WebApplication.CreateBuilder(args);



builder.Logging.ClearProviders();
builder.Logging.AddConsole();

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();



// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();

    app.UseMiddleware<ExceptionCustomMiddlewares>();

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();

    app.Run();

}

/*
 app.ExceptionCustomMiddlewares();
//app.UseExceptionHandler("/error");
    
//app.Environment.EnvironmentName = "Production"; // меняем имя окружения
 *
 * 
 
 *app.Map("/error", app => app.Run(async context =>
{
    context.Response.StatusCode = 500;
    Console.WriteLine("Error 500. An exception occurred for a task in HW=28!");
    
    await context.Response.WriteAsync("Error 500. An exception occurred for a task in HW=28!");
}));

 * //app.UseDeveloperExceptionPage();

//app.Map( );

//app.Run(); app.UseExceptionHandler("/Error")

/*app.Run(async (context) =>
{
    int a = 5;
    int b = 0;
    int c = a / b;
    await context.Response.WriteAsync($"c = {c}");
});
*/
