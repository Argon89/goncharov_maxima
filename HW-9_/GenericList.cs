﻿using System;
using System.Linq.Expressions;

namespace HW_9
{
    public class GenericList<T> where T : new()
    {
        public GenericList(int index)
        {
            _index = index - 1;
            arr = new T[index];
            ArrInit(index);
        }

        private int _index;
        private T[] arr;

       
        /*  
        public T  this[int i]
        {
            get => arr[i];
            set => arr[i] = value;
        }
        */
        private void ArrInit(int _i)
        {
            if (_i <= 0) throw new ArgumentOutOfRangeException(nameof(_i));


            for (int i = _i - 1; i >= 0; i--)
            {
                arr[i] = new T();
            }
        }

        public void Add(T item)
        {
            if (_index >= 0)
            {
                arr[_index--] = item;
                //_index--;
            }
            else
            {
                Console.WriteLine("The array is full");
            }
        }

        public void Print()
        {
            int i = 0;
            foreach (var T in arr)
            {
                Console.Write($"object {i} value = ");
                Console.WriteLine($"{T} ");
                i++;
            }
        }
    }
}