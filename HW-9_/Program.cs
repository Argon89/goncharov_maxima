﻿using System;

namespace HW_9
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            
            int i = 0;
            Console.WriteLine("Enter the quantity of objects");
            int index = int.Parse(Console.ReadLine());
            var objectList = new GenericList<int>(index);
            var PrinterObject = new Printer();

            while (i <= index - 1)
            {
                Console.WriteLine("enter the object value");
                //objectList[i] = int.Parse(Console.ReadLine());  
                var read = int.Parse(Console.ReadLine());
                objectList.Add(read);
                i++;
            }

            PrinterObject.Print<string>(nameof(objectList));
            objectList.Print();
        }
    }
}