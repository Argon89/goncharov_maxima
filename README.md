ветка HW-2

# Гончаров Аркадий. Backend c# 
Домашнее задание
● Реализовать generic класс GenericList<T> с методами Add(T item) и
Print(). Внутри класса реализовать массив объектов (T[]), в который 
можно добавить элемент через метод Add. Метод Print выводит в 
консоль все элементы. Количество элементов задается в конструкторе. 
Если вызвать метод Add больше раз, чем количество элементов в 
массиве - выводить предупреждение в консоли.
● Создать класс наследник GenericList<T>
● Реализовать класс Printer с generic методом Print<T>(T item), где T
должен быть классом. Метод выводит в консоль строковое 
представление объекта. (obj.ToString)
● Реализовать примеры использования созданных классов в консоли



Домашнее задание
● Создать класс, описывающий пылесос
○ В классе должен быть виртуальное свойство Model для получения наименования модели пылесоса
○ В классе должен быть виртуальный метод StartCleaning, который пишет в консоль о том, что началась 
уборка
○ Создать перегрузку метода StartCleaning, которая принимает параметром комнату, в которой нужно 
сделать уборку
● Создать 3 класса наследника 
○ Например: обычный пылесос, робот пылесос, моющий пылесос и тд
○ В 1 наследнике оставить тело метода StartCleaning родителя
○ В 2 наследнике переопределить метод StartCleaning, написав в начале уборки о том, какая модель 
пылесоса начала уборку
○ В 3 наследнике реализовать сокрытие метода StartCleaning
● Каждый класс описать в отдельном файле
● Создать массив базового типа, добавить в него объекты класса пылесос. В цикле у 
всех объектов вызвать метод StartCleaning
