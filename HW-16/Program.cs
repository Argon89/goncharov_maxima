﻿
using System.Linq;
using HW_16;

Console.WriteLine($"=============== Первая часть задания");
List<int> arr1 = new List<int>() {22, 99, 122, 77, 54, 45, 267, 900, 12, 22, 99, 122};
Console.WriteLine($"=============== до преобразования");
foreach (var arr in arr1)
{
    Console.Write($"{arr}, ");
}

var oddInts = arr1.Where(i => i % 2 != 0).Distinct();

Console.WriteLine($@"
=================удалены чётные числа и повторы");
foreach (var arr in oddInts)
{
    Console.Write($"{arr}, ");
}
//////////////////////////////////////////////////////////////////////////////////////////
Console.WriteLine($@"
=============== Вторая часть задания");
Console.WriteLine($"=============== до преобразования");
foreach (var arr in arr1)
{
    Console.Write($"{arr}, ");
}

/*var odd2 = arr1.Where(i => i % 2 != 0);
var newList = odd2.Select(x => x.ToString()).OrderBy(x => x);*/

var odd2 = arr1.Where(i => i % 2 != 0).Select(x => x.ToString()).OrderBy(x => x);

Console.WriteLine($@"
=================.ToString()");
foreach (var arr in odd2)
{
    Console.Write($"{arr}, ");
}
//////////////////////////////////////////////////////////////////////////////////////////////
Console.WriteLine($@"
=============== Третяя часть задания");
Console.WriteLine($@"
Клиенты фитнес центра ");
List<Client> clients = new List<Client>();
clients.Add(new Client(){CodeID = 0, Year = 2020, Month = 10, Duration = 55});
clients.Add(new Client(){CodeID = 1, Year = 2021, Month = 2, Duration = 55});
clients.Add(new Client(){CodeID = 2, Year = 2018, Month = 6, Duration = 68});
clients.Add(new Client(){CodeID = 3, Year = 2015, Month = 9, Duration = 155});
clients.Add(new Client(){CodeID = 4, Year = 2019, Month = 3, Duration = 160});

foreach (var client in clients)
{
    Console.WriteLine(($"Продолжительность занятий: {client.Duration}, год: {client.Year}, месяц: {client.Month}, Код клиента: {client.CodeID} "));
}



var BadClient = clients
    .Where(cust => cust.Duration ==
                   (clients.MinBy(cust => cust.Duration)).Duration)
    .Select(cust =>
        $"Самый новый клиент с минимальной подолжительностью занятий: {cust.Duration};  Дата регистрации: год:{cust.Year}, месяц: {cust.Month}.")
    .Last();


Console.WriteLine(BadClient);

/*

*/