﻿namespace HW_16;

public class Client
{
    public int CodeID { get; set; }
    public int Year { get; set; }
    public int Month { get; set; }
    public int Duration { get; set; }
}