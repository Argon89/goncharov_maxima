﻿
await PourCoffee();
await Task.WhenAll(FryEggs(), FryBacon(), TostBread());

await PourJuise();

async Task PourCoffee()
{
    Console.WriteLine("Готовим кофе");
    await Task.Delay(2000);
    Console.WriteLine("Кофе готов");
}
async Task FryEggs()
{
    Console.WriteLine("Жарим яичницу");
    await Task.Delay(7000);
    Console.WriteLine("Яичница готова");
}

async Task FryBacon()
{
    Console.WriteLine("Жарим бекон");
    await Task.Delay(5000);
    Console.WriteLine("Беокон готов");
}

async Task TostBread()
{
    Console.WriteLine("Жарим тосты");
    await Task.Delay(1000);
    Console.WriteLine("Тосты готовы");
    await JamOnBread();
}

async Task JamOnBread()
{
    Console.WriteLine("Мажем джем на тосты");
    await Task.Delay(7750);
    Console.WriteLine("Джем намазан");
}

async Task PourJuise()
{
    Console.WriteLine("Наливаем сок");
    await Task.Delay(3750);
    Console.WriteLine("Сок налит");
}

//event  FryHandler FryStatus;
//delegate Task FryHandler(string message); 
