﻿namespace HW_12;

class Program
{
    private static readonly Card2 TransportCard = new Card2();

    public static void Main()
    {
        TransportCard.PayMessage += ShowOperation;

        TransportCard.Replenishment(new Random().Next(1, 60));
        
        TransportCard.Pay();
        
        void ShowOperation(string message)
        {
            Console.WriteLine(message);
        }
    }
}


/*

System.Threading.Thread.Sleep(5000);
        
*/